// implement kmeans algorithms
#include "kmeans.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#define malloc2D(objects, xDim, yDim, type) do {		\
	objects = (type **)malloc(xDim* sizeof(type *)); 	\
	objects[0] = (type *)malloc(xDim*yDim *sizeof(type));\
	for (size_t i = 1;i<xDim; i++)						\
		objects[i] = objects[i-1] + yDim;				\
}while(0)

__device__ float euclid_distance_2(float *objects, float *clusters, int numCoord){
	float ans = 0.0f;
	for (int i = 0;i<numCoord; i++)
		ans += (objects[i]-clusters[i])*(objects[i]-clusters[i]);
	return ans;
}
__global__ void find_nearest_cluster(float *deviceObject, float *deviceClusters,float *deviceSumCluster,int*count,int *deviceMembership, int numObjs, int numCoord, int numCluster){
	int index = threadIdx.x + blockIdx.x*blockDim.x;
	float min_dist = FLT_MAX;
	float temp;
	for (int i =0 ;i<numCluster;i++){
		temp = euclid_distance_2(&deviceObject[index*numCoord], &deviceClusters[i*numCoord],numCoord);
//		printf("index: %i with cluseter: %i have %f \n",index, i, temp );
		if(temp < min_dist){
			min_dist = temp;
			deviceMembership[index] = i;
		}
	}
	int res_cluster = deviceMembership[index];
	for (int j =0 ;j<numCoord;j++){
		atomicAdd(&deviceSumCluster[res_cluster*numCoord + j], deviceObject[index*numCoord+j]);
	}
	atomicAdd(&count[res_cluster],1);
}

__global__ void cal_new_centroid(float *deviceClusters,float*deviceSumCluster,int* count, int numCoord){
	int index =  threadIdx.x + blockIdx.x*blockDim.x;
	for(int i =0;i<numCoord;i++){
		deviceClusters[index*numCoord + i] = deviceSumCluster[index*numCoord +i]/count[index];
//		printf("%i:%i new device %f / %i = %f\n",index, i,deviceSumCluster[index*numCoord + i], count[index], deviceClusters[index + i]);
	}
}
/*data layout
object 		[numbObj][numbCoord]
objects 	[numCoords][numObjsts]
clusters 	[numbCoords][numCluster]
membership 	[numbCoords]
pre_cluster [numCoords]
*/
__global__ void checkCluster(int* membership, int *devicePreMem, int numObjs,int power2ofnumObj, int *res_check){// use reduction 

	extern __shared__ unsigned int intermediates[];

    intermediates[threadIdx.x] =
        (threadIdx.x < numObjs) ? (membership[threadIdx.x]!= devicePreMem[threadIdx.x] ?1:0) : 0;

    __syncthreads();


    for (unsigned int s = power2ofnumObj / 2; s > 0; s >>= 1) {
        if (threadIdx.x < s) {
            intermediates[threadIdx.x] += intermediates[threadIdx.x + s];
        }
        __syncthreads();
    }
//    printf("thread %i have %i membership %i vs devicePreMem %i\n", threadIdx.x, intermediates[threadIdx.x], membership[threadIdx.x], devicePreMem[threadIdx.x]);
    if (threadIdx.x == 0) {
        res_check[0] = intermediates[0];
    }
}
void printCudaErr(){
	cudaError_t e = cudaGetLastError();
	if(e != cudaSuccess){
		fprintf(stderr,"Cuda error %d: %s\n", e, cudaGetErrorString(e));
		exit(1);
	}
}
void checkCuda(cudaError_t e){
	if(e!= cudaSuccess){
		fprintf(stderr,"Cuda error %d: %s\n", e, cudaGetErrorString(e));
		exit(1);
	}
}
void printMembershipt(int*membership, int numObjs){
	for (int i = 0 ;i<numObjs; i++)
		printf("number %d: in cluster %d\n", i, membership[i]);
}
void printHostCluster(float*hostCluster,int numCoord,int numClusters){
	printf("Host cluster\n");
	for(int i =0;i<numClusters;i++){
		for(int j = 0;j<numCoord;j++)
			printf("%f\t",hostCluster[i*numCoord + j]);
		printf("\n");
	}
}
void cuda_kmeans(float ** objects, int numObjs, int numCoord, int numClusters, int * membership, int *loop_iterations){
//	int *membership;
	float **dimclusters;
	int *hostcount;
	float *hostCluster;

	float *deviceObject;
	float *deviceClusters;
	float *deviceSumCluster;
	int *count;
	int *deviceMembership;
	int *hostPreMem;
	int *devicePreMem;




	malloc2D(dimclusters, numClusters, numCoord,float);
	// init deviceCluster
    for (int i = 0; i < numClusters; i++) 
        for (int j = 0; j < numCoord; j++) 
            dimclusters[i][j] = objects[i][j];


	hostPreMem = (int *)malloc(numObjs* sizeof(int));
	for(int i = 0;i<numObjs;i++)
		hostPreMem[i] = -1;
	hostcount = (int*)malloc(numClusters*sizeof(int));
	hostCluster = (float*)malloc(numClusters*numCoord*sizeof(float));


	// malloc device memory
	cudaMalloc(&deviceObject, numObjs*numCoord*sizeof(float));
	cudaMalloc(&deviceClusters, numCoord*numClusters*sizeof(float));
	cudaMalloc(&deviceSumCluster, numCoord*numClusters*sizeof(float));
	cudaMalloc(&deviceMembership, numObjs* sizeof(int));
	cudaMalloc(&devicePreMem, numObjs*sizeof(int));
	cudaMalloc(&count, numClusters*sizeof(int));

	cudaMemcpy(deviceObject, objects[0], numObjs*numCoord*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(deviceMembership, membership, numObjs*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(deviceClusters, dimclusters[0], numCoord*numClusters*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(devicePreMem, hostPreMem, numObjs*sizeof(int),cudaMemcpyHostToDevice);
	int loop =0;
	int res_check ;
	int power2ofnumObj ,temp =1;
	for(int i =0;i<numObjs;i++){
		temp = 2*temp;
		if(temp>=numObjs)
			break;
	}
	power2ofnumObj = temp;

	for(int i =0;i<numClusters;i++){
		hostcount[i] = 0;
	}
	for(int i = 0;i<numCoord*numClusters;i++)
		hostCluster[i] = 0;
	do{

		cudaMemcpy(count, hostcount, numClusters*sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(deviceSumCluster, hostCluster, numCoord*numClusters*sizeof(float),cudaMemcpyHostToDevice);
		find_nearest_cluster<<< 1,numObjs>>>(deviceObject, deviceClusters,deviceSumCluster,count, deviceMembership, numObjs, numCoord, numClusters);
		cudaDeviceSynchronize(); printCudaErr();
		cal_new_centroid<<<1,numClusters>>>(deviceClusters, deviceSumCluster, count, numCoord);


		checkCluster<<<1,power2ofnumObj, power2ofnumObj*sizeof(int)>>>(deviceMembership, devicePreMem, numObjs,power2ofnumObj,count);
		cudaMemcpy(&res_check, count, sizeof(int), cudaMemcpyDeviceToHost);
		if(!res_check){	
			break;
		}	
		cudaDeviceSynchronize(); printCudaErr();
		cudaMemcpy(devicePreMem, deviceMembership,numObjs*sizeof(int), cudaMemcpyDeviceToDevice);
/*
		// debug
		cudaMemcpy(hostCluster, deviceClusters, numCoord*numClusters*sizeof(float),cudaMemcpyDeviceToHost);

		printHostCluster(hostCluster,numCoord,numClusters);

		cudaMemcpy(membership, deviceMembership, numObjs*sizeof(int), cudaMemcpyDeviceToHost);
		printMembershipt(membership,numObjs);*/
	}while(loop++ <50 );

	*loop_iterations = loop ;
	cudaMemcpy(membership, deviceMembership, numObjs*sizeof(int), cudaMemcpyDeviceToHost);
	free(dimclusters);
	free(hostcount);
	free(hostCluster);
	free(hostPreMem);

	cudaFree(deviceObject);
	cudaFree(deviceMembership);
	cudaFree(deviceClusters);
	cudaFree(devicePreMem);
	cudaFree(deviceSumCluster);
	cudaFree(count);

}