#include "kmeans.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define MAX_CHAR_PER_LINE 128


void printObjects(float **Objects,int numObjs, int numCoords){
	int i,j;
	for ( i =0;i<numObjs;i++){
		for ( j = 0; j < numCoords; j++)
			printf("%f\t",Objects[i][j]);
		printf("\n");
	}
}

void file_write(char *filename, int* membership, int numObjs, int numClusters){
	FILE * fptr;
	char outFileName[1024];
	sprintf(outFileName,"%s.membership", filename);
	printf("Writing membership of N=%d data objects to file \"%s\"\n",numObjs, outFileName);
	fptr = fopen(outFileName, "w");
	int i;
	for( i = 0;i<numObjs;i++)
		fprintf(fptr, "%d %d\n", i, membership[i]);
	fclose(fptr);
}

float** file_read(char *filename, int  *numObjs,int  *numCoords){
    float **objects;
    int     i, j, len;
    ssize_t numBytesRead;

    FILE *infile;
    char *line, *ret;
    int   lineLen;

    if ((infile = fopen(filename, "r")) == NULL) {
        fprintf(stderr, "Error: no such file (%s)\n", filename);
        return NULL;
    }

    /* first find the number of objects */
    lineLen = MAX_CHAR_PER_LINE;
    line = (char*) malloc(lineLen*sizeof(char));
    assert(line != NULL);

    (*numObjs) = 0;
    while (fgets(line, lineLen, infile) != NULL) {
        /* check each line to find the max line length */
        while (strlen(line) == lineLen-1) {
            /* this line read is not complete */
            len = strlen(line);
            fseek(infile, -len, SEEK_CUR);

            /* increase lineLen */
            lineLen += MAX_CHAR_PER_LINE;
            line = (char*) realloc(line, lineLen);
            assert(line != NULL);

            ret = fgets(line, lineLen, infile);
            assert(ret != NULL);
        }

        if (strtok(line, " \t\n") != 0)
            (*numObjs)++;
    }
    rewind(infile);
    if (_debug) printf("lineLen = %d\n",lineLen);

    /* find the no. objects of each object */
    (*numCoords) = 0;
    while (fgets(line, lineLen, infile) != NULL) {
        if (strtok(line, " ,\t\n") != 0) {
            /* ignore the id (first coordiinate): numCoords = 1; */
            while (strtok(NULL, " ,\t\n") != NULL) (*numCoords)++;
            break; /* this makes read from 1st object */
        }
    }
    rewind(infile);
    if (_debug) {
        printf("File %s numObjs   = %d\n",filename,*numObjs);
        printf("File %s numCoords = %d\n",filename,*numCoords);
    }

    /* allocate space for objects[][] and read all objects */
    len = (*numObjs) * (*numCoords);
    objects    = (float**)malloc((*numObjs) * sizeof(float*));
    assert(objects != NULL);
    objects[0] = (float*) malloc(len * sizeof(float));
    assert(objects[0] != NULL);
    for (i=1; i<(*numObjs); i++)
        objects[i] = objects[i-1] + (*numCoords);

    i = 0;
    /* read all objects */


	char* tok;
	for ( i = 0 ;i< (*numObjs); i++){
		if (fgets(line, lineLen, infile)!= NULL){
			tok = strtok(line, ",\t\n");
			for ( j = 0; j< (*numCoords); j++){
//				printf("%s\n", tok);
				objects[i][j] = atof(tok);
//				printf("%f\n", atof(tok));
				tok = strtok(NULL,",\t\n");
			}
			strtok(NULL, ",\t\n");
		}
	}


    fclose(infile);
    free(line);

    return objects;
}