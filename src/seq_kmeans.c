// implement kmeans algorithms
#include "kmeans.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <stdbool.h>

#define malloc2D(objects, xDim, yDim, type) do {		\
	objects = (type **)malloc(xDim* sizeof(type *)); 	\
	objects[0] = (type *)malloc(xDim*yDim *sizeof(type));\
	size_t i;											\
	for (i = 1;i<xDim; i++)						\
		objects[i] = objects[i-1] + yDim;				\
}while(0)

float euclid_distance_2(float *objects, float *clusters, int numCoord){
	float ans = 0.0f;
	int i;
	for ( i = 0;i<numCoord; i++)
		ans += (objects[i]-clusters[i])*(objects[i]-clusters[i]);
	return ans;
}


void seq_kmeans(float ** objects, int numObjs, int numCoord, int numClusters, int * membership, int *loop_iterations){
//	int *membership;
	float **clusters, **sumCluster;
	int *count;


	malloc2D(clusters, numClusters, numCoord,float);
	// init deviceCluster
	int i,j;
    for (i = 0; i < numClusters; i++) 
        for (j = 0; j < numCoord; j++) 
            clusters[i][j] = objects[i][j];
    malloc2D(sumCluster, numClusters, numCoord, float);


	for( i = 0;i<numObjs;i++)
		membership[i] = -1;
	count = (int*)malloc(numClusters*sizeof(int));
	int new_mem, old_mem;
	int loop =0;
	float res_euclid;
	bool stop_point;
	do{
		stop_point = true;
		int i,j;
		for(i = 0;i<numClusters;i++){
			count[i] = 0;
			for( j = 0;j<numCoord;j++)
				sumCluster[i][j] = 0;
		}

		for( i = 0;i<numObjs;i++){
			float min_dist = FLT_MAX;
			old_mem = membership[i];
			for( j = 0;j<numClusters;j++){
				res_euclid = euclid_distance_2(objects[i], clusters[j],numCoord);
				if(res_euclid < min_dist){
					min_dist = res_euclid;
					membership[i] = j;
				}
			}
			new_mem = membership[i];
			if(new_mem != old_mem)
				stop_point = false;
			count[new_mem] +=1;
			for( j = 0;j<numCoord;j++)
				sumCluster[new_mem][j] += objects[i][j];
		}
		for( i=0;i<numClusters;i++)
			for( j = 0;j<numCoord;j++)
				clusters[i][j]  = sumCluster[i][j]/count[i];
/*		printMembershipt(membership, numObjs);
		printf("NUMBERLOOP: %d\n", loop);*/
		if(stop_point)
			break;
	}while(loop++ <15 );

	*loop_iterations = loop ;
	free(clusters);
	free(sumCluster);
	free(count);


}

void printMembershipt(int*membership, int numObjs){
	int i;
	for (i = 0 ;i<numObjs; i++)
		printf("number %d: in cluster %d\n", i, membership[i]);
}
void printHostCluster(float*hostCluster,int numCoord,int numClusters){
	printf("Host cluster\n");
	int i,j;
	for( i =0;i<numClusters;i++){
		for( j = 0;j<numCoord;j++)
			printf("%f\t",hostCluster[i*numCoord + j]);
		printf("\n");
	}
}