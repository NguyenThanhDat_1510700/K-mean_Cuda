#include "kmeans.h"
#include <stdlib.h>
#include <string.h> 
#include <time.h>
#include <unistd.h>     /* getopt() */

int _debug;
static void usage(char *argv0) {
    char *help =
        "Usage: %s  -i filename -n num_clusters\n"
        "       -i filename    : file containing data to be clustered\n"
        "       -n num_clusters: number of clusters (K must > 1)\n";
    fprintf(stderr, help, argv0);
    exit(-1);
}
int main(int argc,char ** argv){
	int opt;
	float **objects;
	int numObjs, numCoords;
	int numClusters ;
	int loop;
	int *membership;
	char* filename;
   	clock_t  timing, io_timing, clustering_timing;

   	filename = NULL;
   	numClusters = 3;
	_debug = 0;


    while ( (opt=getopt(argc,argv,"p:i:n:t:abdo"))!= EOF) {
        switch (opt) {
            case 'i': filename=optarg;
                      break;
            case 'n': numClusters = atoi(optarg);
                      break;
            case '?': usage(argv[0]);
                      break;
            default: usage(argv[0]);
                      break;
        }
    }
    io_timing = clock();
    if (filename == 0 || numClusters <= 1) usage(argv[0]);

	objects = file_read(filename, &numObjs, &numCoords);
	if (_debug)
		printObjects(objects, numObjs, numCoords);
	if(objects ==NULL) exit(1);
    timing            = clock();
    io_timing         = (timing - io_timing);
    clustering_timing = timing;


	membership = (int* )malloc(numObjs *sizeof(int));
	seq_kmeans(objects, numObjs,numCoords,numClusters,membership,&loop);

    timing            = clock();
    clustering_timing = timing - clustering_timing;

    file_write(filename, membership, numObjs, numClusters);
    /*---- output performance numbers ---------------------------------------*/
    io_timing += clock() - timing;
    printf("\nPerforming **** Regular Kmeans (sequential version) ****\n");

    printf("Input file:     %s\n", filename);
    printf("numObjs       = %d\n", numObjs);
    printf("numCoords     = %d\n", numCoords);
    printf("numClusters   = %d\n", numClusters);

    printf("Loop iterations    = %d\n", loop);

    printf("I/O time           = %10.4f sec\n", (double)io_timing/ CLOCKS_PER_SEC);
    printf("Computation timing = %10.4f sec\n",(double) clustering_timing/ CLOCKS_PER_SEC);

	free(membership);
	return 0;
}