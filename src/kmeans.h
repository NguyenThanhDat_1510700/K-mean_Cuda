#ifndef _H_KMEANS
#define _H_KMEANS

#include <assert.h>
#include <stdio.h>

float ** file_read(char*, int*, int*);
void 	 file_write(char*,int*, int, int );

extern int _debug;

void printObjects(float **, int, int);

void cuda_kmeans(float **, int, int, int, int *, int *);

void printCudaErr();

void seq_kmeans(float**, int, int, int, int*,int*);


#endif