#include "kmeans.h"
#include <stdio.h>
#include <string.h>


#define MAX_CHAR_PER_LINE 128

float** _file_read(char*filename, int*numObjs, int * numCoords){
	int _debug = 1;
	float **objects;
	int len;
	FILE *infile;
	char *line, *ret;
	int lineLen;
	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error: no such file %s\n", filename);
		return NULL;
	}
	// get number of object 
	lineLen = MAX_CHAR_PER_LINE;
	line = (char *) malloc(lineLen);
	assert(line != NULL);

	(*numObjs) =0;
	char c;
	while((c = fgetc(infile))!= EOF){
		if (c == '\n')
			(*numObjs)++;
	}
	(*numObjs)++;
	rewind(infile);
	if (_debug) printf("lineLen = %d\n", lineLen);

	// get number of attribute of each object
	(*numCoords ) = 0;
	if (fgets(line, lineLen, infile) != NULL){
//		printf("%s size%d\n", line, strlen(line));
		char *tok = strtok(line, ",\t\n");
		if(tok != NULL){
//			printf("%s\n", tok);
			while (strtok(NULL, ",\t\n")!= NULL) {
				(*numCoords)++;
			}
		}
	}
	if(_debug) printf("FIle %s numObjs %d numCoords %d \n", filename, *numObjs, *numCoords);

	// allocate memory for object[][] read all object
	len = (*numCoords) * (*numObjs);
	objects = (float ** )malloc((*numObjs)* sizeof(float*));
	assert(objects != NULL);
	objects[0] = (float *) malloc((*numCoords)* sizeof(float));
	assert(objects[0] != NULL);
	for (int i =1; i< (*numObjs);i++){
		objects[i] = objects[i-1] + *numCoords;
	}
	objects[0][0] = 4.6f;
	rewind(infile);
	// read file and assign data
	

	char* tok;
	for (int i = 0 ;i< (*numObjs); i++){
		if (fgets(line, lineLen, infile)!= NULL){
			tok = strtok(line, ",\t\n");
			for (int j = 0; j< (*numCoords); j++){
//				printf("%s\n", tok);
				objects[i][j] = atof(tok);
				tok = strtok(NULL,",\t\n");
			}
			strtok(NULL, ",\t\n");
		}
	}
	fclose(infile);
	free(line);
	return objects;
}
void printObjects(float **Objects,int numObjs, int numCoords){
	for (int i =0;i<numObjs;i++){
		for (int j = 0; j < numCoords; j++)
			printf("%f\t",Objects[i][j]);
		printf("\n");
	}
}

void file_write(char *filename, int* membership, int numObjs, int numClusters){
	FILE * fptr;
	char outFileName[1024];
	sprintf(outFileName,"%s.membership", filename);
	printf("Writing membership of N=%d data objects to file \"%s\"\n",numObjs, outFileName);
	fptr = fopen(outFileName, "w");
	for(int i = 0;i<numObjs;i++)
		fprintf(fptr, "%d %d\n", i, membership[i]);
	fclose(fptr);
}

float** file_read(  /* flag: 0 or 1 */
                  char *filename,      /* input file name */
                  int  *numObjs,       /* no. data objects (local) */
                  int  *numCoords)     /* no. coordinates */
{
    float **objects;
    int     i, j, len;
    ssize_t numBytesRead;

          FILE *infile;
        char *line, *ret;
        int   lineLen;

        if ((infile = fopen(filename, "r")) == NULL) {
            fprintf(stderr, "Error: no such file (%s)\n", filename);
            return NULL;
        }

        /* first find the number of objects */
        lineLen = MAX_CHAR_PER_LINE;
        line = (char*) malloc(lineLen);
        assert(line != NULL);

        (*numObjs) = 0;
        while (fgets(line, lineLen, infile) != NULL) {
            /* check each line to find the max line length */
            while (strlen(line) == lineLen-1) {
                /* this line read is not complete */
                len = strlen(line);
                fseek(infile, -len, SEEK_CUR);

                /* increase lineLen */
                lineLen += MAX_CHAR_PER_LINE;
                line = (char*) realloc(line, lineLen);
                assert(line != NULL);

                ret = fgets(line, lineLen, infile);
                assert(ret != NULL);
            }

            if (strtok(line, " \t\n") != 0)
                (*numObjs)++;
        }
        rewind(infile);
        if (_debug) printf("lineLen = %d\n",lineLen);

        /* find the no. objects of each object */
        (*numCoords) = 0;
        while (fgets(line, lineLen, infile) != NULL) {
            if (strtok(line, " ,\t\n") != 0) {
                /* ignore the id (first coordiinate): numCoords = 1; */
                while (strtok(NULL, " ,\t\n") != NULL) (*numCoords)++;
                break; /* this makes read from 1st object */
            }
        }
        rewind(infile);
        if (_debug) {
            printf("File %s numObjs   = %d\n",filename,*numObjs);
            printf("File %s numCoords = %d\n",filename,*numCoords);
        }

        /* allocate space for objects[][] and read all objects */
        len = (*numObjs) * (*numCoords);
        objects    = (float**)malloc((*numObjs) * sizeof(float*));
        assert(objects != NULL);
        objects[0] = (float*) malloc(len * sizeof(float));
        assert(objects[0] != NULL);
        for (i=1; i<(*numObjs); i++)
            objects[i] = objects[i-1] + (*numCoords);

        i = 0;
        /* read all objects */


		char* tok;
		for (int i = 0 ;i< (*numObjs); i++){
			if (fgets(line, lineLen, infile)!= NULL){
				tok = strtok(line, ",\t\n");
				for (int j = 0; j< (*numCoords); j++){
	//				printf("%s\n", tok);
					objects[i][j] = atof(tok);
					tok = strtok(NULL,",\t\n");
				}
				strtok(NULL, ",\t\n");
			}
		}
/*        while (fgets(line, lineLen, infile) != NULL) {
            if (strtok(line, " \t\n") == NULL) continue;
            for (j=0; j<(*numCoords); j++)
                objects[i][j] = atof(strtok(NULL, " ,\t\n"));
            i++;
        }*/

        fclose(infile);
        free(line);

    return objects;
}