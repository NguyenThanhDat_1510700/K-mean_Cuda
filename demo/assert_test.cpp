#include <stdio.h>
#include <assert.h>

void print_number(int* a){
	assert(a);
	printf("%d\n", *a);
}
int main(){
	int a =10;
	int *b = NULL;
	int *c = NULL;
	b = &a;
	print_number(b);
	print_number(c);
	return 0;
}